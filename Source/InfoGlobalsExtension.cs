using System.Runtime.CompilerServices;
using TenCrowns.GameCore;

namespace BonusUnitFamily {
  public static class InfoGlobalsExtension {
    private static readonly ConditionalWeakTable<InfoGlobals, InfoGlobalsAdditionalData> data =
      new ConditionalWeakTable<InfoGlobals, InfoGlobalsAdditionalData>();

    public static InfoGlobalsAdditionalData AdditionalData(this InfoGlobals infoGlobals) {
      return data.GetOrCreateValue(infoGlobals);
    }
  }
}