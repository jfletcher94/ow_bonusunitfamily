﻿using System;
using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using TenCrowns.AppCore;
using TenCrowns.GameCore;
using TenCrowns.GameCore.Text;

namespace BonusUnitFamily {
  public class BonusUnitFamily : ModEntryPointAdapter {
    public const string MY_HARMONY_ID = "Thremtopod.BonusUnitFamily.patch";
    public static Harmony harmony;
    public static MethodInfo game;

    public override void Initialize(ModSettings modSettings) {
      if (harmony != null) {
        return;
      }

      game = AccessTools.Method(typeof(Player), "game");
      harmony = new Harmony(MY_HARMONY_ID);
      harmony.PatchAll();
      CustomXmlTags.CustomXmlTags.AppendGlobals<InfoGlobalsAdditionalData>(globals => globals.AdditionalData());
    }

    public override void Shutdown() {
      if (harmony == null) {
        return;
      }
      harmony.UnpatchAll(MY_HARMONY_ID);
      harmony = null;
    }
  }

  [HarmonyPatch(typeof(Player))]
  public class PatchPlayer {
    [HarmonyPrefix]
    [HarmonyPatch(
      typeof(Player), "doBonus", typeof(BonusType), typeof(Dictionary<PlayerType, TextBuilder>), typeof(Tile),
      typeof(Unit), typeof(City), typeof(Character), typeof(TraitType), typeof(OccurrenceType), typeof(GoalType),
      typeof(ResourceType), typeof(TheologyType), typeof(LawType), typeof(TechType), typeof(FamilyType),
      typeof(ReligionType), typeof(TribeType), typeof(PlayerType), typeof(List<object>), typeof(bool),
      typeof(Func<TextVariable>))]
    static bool PrefixDoBonus(Player __instance, BonusType eBonus) {
      InfoGlobalsAdditionalData data = ((Game) BonusUnitFamily.game.Invoke(__instance, null)).infos()
        .Globals.AdditionalData();
      if (__instance.getNumFamilies() < 2) {
        return true;
      }
      if (eBonus == data.TECH_ADMINISTRATION_BONUS_WORKER_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_WORKER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_DRAMA_BONUS_SETTLER_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_SETTLER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_SPOKED_WHEEL_BONUS_CHARIOT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_CHARIOT_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_NAVIGATION_BONUS_BIREME_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_BIREME_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_PORTCULLIS_BONUS_MACEMAN_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_MACEMAN_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_LAND_CONSOLIDATION_BONUS_CAMEL_ARCHER_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_CAMEL_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_LAND_CONSOLIDATION_BONUS_WAR_ELEPHANT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_WAR_ELEPHANT_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_COMPOSITE_BOW_BONUS_ARCHER_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_MACHINERY_BONUS_ONAGER_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_ONAGER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_STIRRUPS_BONUS_HORSEMAN_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_HORSEMAN_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_STIRRUPS_BONUS_HORSE_ARCHER_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_HORSE_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_HYDRAULICS_BONUS_BALLISTA_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_BALLISTA_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_CARTOGRAPHY_BONUS_TRIREME_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_TRIREME_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_LATEEN_SAIL_BONUS_DROMON_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_DROMON_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_BODKIN_ARROW_BONUS_LONGBOWMAN_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_LONGBOWMAN_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_WINDLASS_BONUS_CROSSBOWMAN_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_CROSSBOWMAN_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_BATTERING_RAM_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_BATTERING_RAM_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_SIEGE_TOWER_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_SIEGE_TOWER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_AKKADIAN_ARCHER_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_AKKADIAN_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_CIMMERIAN_ARCHER_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_CIMMERIAN_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_AFRICAN_ELEPHANT_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_AFRICAN_ELEPHANT_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_TURRETED_ELEPHANT_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_TURRETED_ELEPHANT_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_LIGHT_CHARIOT_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_LIGHT_CHARIOT_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_KUSHITE_CAVALRY_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_KUSHITE_CAVALRY_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_HOPLITE_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_HOPLITE_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_PHALANGITE_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_PHALANGITE_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_PALTON_CAVALRY_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_PALTON_CAVALRY_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_CATAPHRACT_ARCHER_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_CATAPHRACT_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_HASTATUS_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_HASTATUS_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_LEGIONARY_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_LEGIONARY_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_HITTITE_CHARIOT_1_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_HITTITE_CHARIOT_1_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_HITTITE_CHARIOT_2_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_HITTITE_CHARIOT_2_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_MEDJAY_ARCHER_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_MEDJAY_ARCHER_EVENTTRIGGER);
      }
      if (eBonus == data.TECH_BEJA_ARCHER_UNIT_BONUS) {
        return !__instance.doEventTrigger(data.TECH_BONUS_UNIT_BEJA_ARCHER_EVENTTRIGGER);
      }
      return true;
    }
  }
}